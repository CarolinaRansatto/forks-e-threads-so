#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>

void f1(int* origem)
{
    if ((*origem) == 1)
        printf("Thread 1 do processo filho\n");
    else
        printf("Thread 1 do processo pai\n");
}

void f2(int* origem)
{
    if ((*origem) == 1)
        printf("Thread 2 do processo filho\n");
    else
        printf("Thread 2 do processo pai\n");
}

int main()
{
    pthread_t t1, t2;
    int filho = 1, pai = 2;
    pid_t pid = fork();
    
    if (pid < 0)
        exit(1);
    if (pid == 0) {
        pthread_create(&t1, NULL, (void*)f1, &filho);
        pthread_create(&t2, NULL, (void*)f2, &filho);
    }
    else {
        pthread_create(&t1, NULL, (void*)f1, &pai);
        pthread_create(&t2, NULL, (void*)f2, &pai);
    }
    
    pthread_join(t2, NULL);
    pthread_join(t1, NULL);
    
    return 0;
}
